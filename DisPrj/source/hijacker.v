`timescale 1ns / 1ps


module hijacker
#(parameter
CameraDataDepth=16,//Data from camera
//Set the width parameter for window extraction module
ImageW=640,
ImageH=480,

dispLevel=64,
DataDepth=16,
DataDiffDepth=7,
SGMDataDepth=6
)

(
	 input fclk,
	 input clk1,
	 input clk2,
	 
	 input CamADV,
	 input FbWrARst,
	 input CamBDV,
	 input FbWrBRst,
	 
	  
    input [CameraDataDepth-1:0] DI1,
    input [CameraDataDepth-1:0] DI2,
    input [CameraDataDepth-1:0] DIx,
    output [CameraDataDepth-1:0] DO1,
    output [CameraDataDepth-1:0] DO2,
	 output [7:0]LED_O,
	 input [7:0]SW_I,
	 output [7:0]IO_O,
	 
	 
	 input sck,input css,input mosi,inout miso,
	 output spi_DatRdy,output spi_ScreenRst
    );
	 wire miso_;
	 
	 reg misoGate;
	 assign miso = (misoGate) ? miso_ : 1'bz;
	 
	 wire clk_p=clk2;
	 always@(posedge clk_p)misoGate<=css;
	 
	 
	reg [8-1:0]SaData[7:0];
	 
	
	wire [8-1:0]dat_i;
	wire [8-1:0]ndat_i;
	wire mosi;
	wire spi_rdy;
	wire spi_prerdy;
	
	wire sck_posedge;
	reg [8-1:0]feedData;
	

	wire en_p=CamBDV;//enD;
	assign spi_DatRdy=en_p;
	wire rst_p=FbWrBRst;
	
	/*
	wire clk_p=clk1;
	wire en_p=CamADV;
	wire rst_p=FbWrARst;*/
	/*
	wire en_p=(SaData[2][0]==0)?CamBDV:enD;//enD;
	assign spi_DatRdy=en_p;
	wire rst_p=(SaData[2][0]==0)?FbWrBRst:spi_ScreenRst;*/
	/*
	
	SPI_slave SPI_S1(clk_p,sck,css, mosi,miso_,
	feedData,dat_i,ndat_i,sck_posedge,accep_dat_o,spi_rdy,spi_prerdy
	);
	
	reg[8*6-1:0]dataG;
	wire SPIByteRdy=sck_posedge&spi_prerdy;
	
	reg enD;
	always@(posedge clk_p)begin//sec for data shift counter 1, 2, 4 
		enD<=SPIByteRdy&Pix_C[5];
	end

	
	
	
	
	always@(posedge clk_p)begin//sec for data shift counter 1, 2, 4 
		if(SPIByteRdy)dataG<={dataG,ndat_i};
	end
	
	reg [2:0]SPI_C;
	always@(posedge clk_p,negedge css)begin//sec for data shift counter 1, 2, 4 
		if(~css)SPI_C<=1;
		else if(SPIByteRdy)SPI_C<={SPI_C,SPI_C[2]};
	end
	
   reg [5:0]Pix_C;
	always@(posedge clk_p,negedge KGate)begin//sec for data shift counter 1, 2, 4 
		if(~KGate)Pix_C<=1;
		else if(SPIByteRdy)Pix_C<={Pix_C,Pix_C[5]};
	end
	
	reg [8*6-1:0]dat_PixK;
	always@(posedge clk_p)begin//sec for data shift counter 1, 2, 4 
		if(SPIByteRdy&Pix_C[5])dat_PixK<={dataG,ndat_i};
	end
	
	
	always@(posedge clk_p,negedge css)begin
		if(~css) KGate=0;
		else if(SPIByteRdy)begin//new spi data byte comes in
			if(KGate)begin
				feedData=spiRet;
			end
			else begin
				feedData=8'hff;
				
				if(SPI_C[1])begin//receive 2 datas 1st data=dat_Pix[0+:8],2nd data=preData , usually for return data 
					if(dataG[0+:8]==16'h81)
						feedData=SaData[ndat_i];
				end
				else if(SPI_C[2])begin//receive 3 datas 1st data=dat_Pix[8+:8],2nd data=dat_Pix[0+:8] 3rd data at , usually for accept data 
					case(dataG[8+:8])
					8'h80:
						SaData[dataG[0+:8]]=ndat_i;
					8'h55:
						KGate=1;
					8'h40:
						spi_ScreenRst=0;
					8'h41:
						spi_ScreenRst=1;
					default:
						KGate=0;
					endcase
				
				end
			end
		end
	end
	
	
	assign LED_O={Pix_C,KGate,spi_ScreenRst};
	
	
	reg KGate,spi_ScreenRst;
	*/
	
	
	wire [10:0]pixX;
	wire [10:0]pixY;

	reg [8:0]spiRet;//=MinDataIdx;
	always@(*)
	begin
		case(SaData[0])
		 0:spiRet<=MinDataIdx*(256/dispLevel);
		 1:spiRet<=DI2r;
		 2:spiRet<=DI1r;
		 3:spiRet<=DIx1;
		 4:spiRet<=(DIx1==0)?0:255;
		 5:spiRet<=(pixX>pixY)?pixX:pixY;
		 default:spiRet<=0;
		 endcase
	
	end
	
	wire [DataDepth-1:0]ColorMean2=(DI2[11+:5]+DI2[0+:5]*2+DI2[5+:6]*2);
	wire [DataDepth-1:0]ColorMean1=(DI1[11+:5]+DI1[0+:5]*2+DI1[5+:6]*2);
	
	wire [DataDepth-1:0]DI2r=ColorMean2;//giv full bit of blue
	
	 
	wire [DataDepth-1:0]DI1r=ColorMean1;//may kick off red
	
	
	
	wire[8-1:0]OR,OG,OB;
	ColorTranse(spiRet,OR,OG,OB);
	assign DO2=(SW_I[7])?{OR[7-:5],OG[7-:6],OB[7-:5]}:DI2;//(pixX[0])?DIx1:DIx2;
	assign DO1=(SW_I[7])?((pixY[3])?DI1:DI2):DI1;
	//assign DO1={ColorMean2[6-:5],ColorMean1[6-:6],ColorMean2[6-:5]};//(pixX[0])?DIx1:DIx2;
	
	//assign IO_O[7:4]={KGate,spi_DatRdy,css&sck,spi_prerdy};
	PixCoordinator # (.frameW(ImageW),.frameH(ImageH)) 
	Pc1(clk_p,en_p,rst_p,pixX,pixY);
	
	
	
	//  7x7 Census outfence window
localparam 
	censusWin=7,
	censusWinCenter=(censusWin-1)/2,
	censusEdgeThick=1,
	censusWinCenter_ArrPos=censusWinCenter*censusWin+censusWinCenter,
	censusVecW=4*(censusWin-censusEdgeThick)*censusEdgeThick;
	//4*(n(w-1)-2*sigma[1~n:k](k-1))=4*(n(w-1)- 2*((1+n)*n/2-n) )
	
	
	
	
	
	wire [censusWin*censusWin*32-1:0]W1;
	
	wire [censusWin*censusWin*32-1:0]W2=W1>>16;
	
	
	localparam grayW=8;
	ScanLWindow_blkRAM #(.block_height(censusWin),.block_width(censusWin)) win1(clk_p,en_p,{DI2r,DI1r},W1);
	
	integer winLi,winLj,winC,OnFenceCounter;
	
	/*
	07@@@@@
	1     @
	2     @
	3     @
	4     @
	5     @
	68@@@@@
	
	
	07E@@@@
	18F   @
	29    @
	3A    @
	4B    @
	5CG   @
	6DG@@@@
	*/
	reg [censusVecW*grayW-1:0]DIx1Win;
	reg [censusVecW*grayW-1:0]DIx2Win;
	always@(*)begin
		OnFenceCounter=0;
		for(winLj=0;winLj<censusWin;winLj=winLj+1)for(winLi=0;winLi<censusWin;winLi=winLi+1)
			if(winLi<censusEdgeThick||winLj<censusEdgeThick||winLi>=censusWin-censusEdgeThick||winLj>=censusWin-censusEdgeThick)begin
				winC=(winLi*censusWin+winLj)*32;
				DIx2Win[OnFenceCounter*grayW+:grayW]=W2[winC+:grayW];
				DIx1Win[OnFenceCounter*grayW+:grayW]=W1[winC+:grayW];
				OnFenceCounter=OnFenceCounter+1;
			end
	end
	
	
	wire [censusVecW-1:0]DIx1_,DIx2_;
	
	CensusVec #(.WinW(censusVecW),.dataInDepth(grayW),.skipIdx(999),.CVPadding(2)) CenV1(DIx1Win,W1[censusWinCenter_ArrPos*32+:grayW],DIx1_);
	CensusVec #(.WinW(censusVecW),.dataInDepth(grayW),.skipIdx(999),.CVPadding(2)) CenV2(DIx2Win,W2[censusWinCenter_ArrPos*32+:grayW],DIx2_);
	
	
	
	
	
	
	reg [censusVecW*(dispLevel)-1:0]PixShiftReg;//To let Left pixel can have Right pixel history to compare
	reg [censusVecW-1:0]DIx1;
	always@(posedge clk_p)if(en_p)begin
		PixShiftReg<={PixShiftReg,DIx2_};
		DIx1<=DIx1_;
	end//pipe stage 1
	
	//AdderTree#(.data_depth(1),.ArrL(16)) AT1({16'b1111111111111111},MinDataIdx);
	
	
	//dir0 : left top
	//dir1 : top
	//dir2 : right top
	//dir3 : left
	
	wire [SGMDataDepth*(dispLevel)-1:0]SGMInfo0;
	wire [SGMDataDepth*(dispLevel)-1:0]SGMInfo1;
	wire [SGMDataDepth*(dispLevel)-1:0]SGMInfo2;
	wire [SGMDataDepth*(dispLevel)-1:0]SGMInfo3;
	
	wire [SGMDataDepth*(dispLevel)-1:0]SGMInfo0Sm;
	wire [SGMDataDepth*(dispLevel)-1:0]SGMInfo1Sm;
	wire [SGMDataDepth*(dispLevel)-1:0]SGMInfo2Sm;
	wire [SGMDataDepth*(dispLevel)-1:0]SGMInfo3Sm;
	
	wire [SGMDataDepth-1:0]minL0,minL1,minL2,minL3;
	
	
	
	
	/*
	
	SGMInfoX-[flipflop]-SGMInfo_dirX_reg_s2(do stuff) and send to delay ele
	*/
	reg [SGMDataDepth*(dispLevel)-1:0]SGMInfo_dir0_reg_s2;//one more stage for 
	reg [SGMDataDepth*(dispLevel)-1:0]SGMInfo_dir1_reg_s2;
	reg [SGMDataDepth*(dispLevel)-1:0]SGMInfo_dir2_reg_s2;
	reg [SGMDataDepth*(dispLevel)-1:0]SGMInfo_dir3_reg_s2;
	
	
	
	getMinIdx  #(.data_depth(SGMDataDepth ),.ArrL(dispLevel))minL0S
	(SGMInfo_dir0_reg_s2,minL0,minL0Idx_);
	
	
	getMinIdx  #(.data_depth(SGMDataDepth ),.ArrL(dispLevel))minL1S
	(SGMInfo_dir1_reg_s2,minL1,minL1Idx_);
	
	getMinIdx  #(.data_depth(SGMDataDepth ),.ArrL(dispLevel))minL2S
	(SGMInfo_dir2_reg_s2,minL2,minL2Idx_);
	
	getMinIdx  #(.data_depth(SGMDataDepth ),.ArrL(dispLevel))minL3S
	(SGMInfo_dir3_reg_s2,minL3,minL3Idx_);
	
	
	ArrayAddSubValue
	#(.dataW(SGMDataDepth),.ArrL(dispLevel),.Add1_Sub0(0)) AAS0
	(SGMInfo_dir0_reg_s2,minL0,SGMInfo0Sm);
	
	
	ArrayAddSubValue
	#(.dataW(SGMDataDepth),.ArrL(dispLevel),.Add1_Sub0(0)) AAS1
	(SGMInfo_dir1_reg_s2,minL1,SGMInfo1Sm);
	
	
	ArrayAddSubValue
	#(.dataW(SGMDataDepth),.ArrL(dispLevel),.Add1_Sub0(0)) AAS2
	(SGMInfo_dir2_reg_s2,minL2,SGMInfo2Sm);
	
	
	ArrayAddSubValue
	#(.dataW(SGMDataDepth),.ArrL(dispLevel),.Add1_Sub0(0)) AAS3
	(SGMInfo_dir3_reg_s2,minL3,SGMInfo3Sm);
	
	
	
	
	
	
	reg [SGMDataDepth*(dispLevel)-1:0]SGMInfo_dir0_reg;
	reg [SGMDataDepth*(dispLevel)-1:0]SGMInfo_dir1_reg;
	reg [SGMDataDepth*(dispLevel)-1:0]SGMInfo_dir3_reg;
	
	
	
	
	
	
	
	
	wire [SGMDataDepth*(dispLevel)-1:0]SGMInfo_dir0=SGMInfo_dir0_reg;
	wire [SGMDataDepth*(dispLevel)-1:0]SGMInfo_dir1=SGMInfo_dir1_reg;
	wire [SGMDataDepth*(dispLevel)-1:0]SGMInfo_dir2=SGMInfo2Sm;
	
	
	
	
	wire [SGMDataDepth*(dispLevel)-1:0]SGMInfo_shiftOut_dir0;
	wire [SGMDataDepth*(dispLevel)-1:0]SGMInfo_shiftOut_dir1;
	wire [SGMDataDepth*(dispLevel)-1:0]SGMInfo_shiftOut_dir2;
	
	wire [SGMDataDepth*(dispLevel)-1:0]SGMInfo_shiftOut_dir3=SGMInfo_dir3_reg;
	
	
	reg [SGMDataDepth*(dispLevel)-1:0]SGMInfo_tmp_d0;
	
	
	//dir3 is just to use privious SGM data, so it needs only 1 delay.
	//dir0,dir2,dir3 data will be send in module ShiftRegWd1 to produce delay effect(SGMInfo_shiftOut_dirX is the video_width-1 delay output of )
	//dir2 is right top pixel from current pixel(that is it needs video_width-1 delay).
	//And module ShiftRegWd1 does is exactly video_width-1 delay, there for dir2 don't need extra delay
	
	
	
	
	
	always@(posedge clk_p)begin 
		if(en_p)begin
			SGMInfo_dir0_reg_s2<=SGMInfo0;
			SGMInfo_dir1_reg_s2<=SGMInfo1;
			SGMInfo_dir2_reg_s2<=SGMInfo2;
			SGMInfo_dir3_reg_s2<=SGMInfo3;
			
			
			
			
			SGMInfo_dir0_reg<=SGMInfo_tmp_d0;
			SGMInfo_tmp_d0<=SGMInfo0Sm;
			SGMInfo_dir1_reg<=SGMInfo1Sm;
			SGMInfo_dir3_reg<=SGMInfo3Sm;
			
			
		end
	end
	reg[10-1:0]addra_bramR;
	always@(posedge clk_p)begin 
		if(en_p)begin
		//addra_bramR:from 0~ImageW-3==> store ImageW-2 data
			if(addra_bramR==ImageW-1-2-1)//we need video_width-1 delay (ImageW-1-1 -1) the extra -1 is because we need one clock to send data to RAM
			addra_bramR=0;
			else
			addra_bramR=addra_bramR+1;
		end
	end
	genvar bri;
	generate
		for(bri=0;bri<dispLevel;bri=bri+1)
		begin
			blkRAM_W32D640_SP ShiftRegWd1(
			.clka(clk_p),
			.ena(en_p),
			.wea(~0),
			.addra(addra_bramR),
			.dina({
			SGMInfo_dir1[bri*SGMDataDepth+:SGMDataDepth],
			SGMInfo_dir0[bri*SGMDataDepth+:SGMDataDepth],
			SGMInfo_dir2[bri*SGMDataDepth+:SGMDataDepth]
			}),
			.douta({
			SGMInfo_shiftOut_dir1[bri*SGMDataDepth+:SGMDataDepth],
			SGMInfo_shiftOut_dir0[bri*SGMDataDepth+:SGMDataDepth],
			SGMInfo_shiftOut_dir2[bri*SGMDataDepth+:SGMDataDepth]}
			)
			
			); 
			
		end
	endgenerate
	
	
	//wire [3:0] adddivThres={SW_I[3:0]};
	localparam SGMDataAggreDepth=SGMDataDepth+2;//4 data Aggregate=> extend 2 bit
	wire [(SGMDataAggreDepth)*(dispLevel)-1:0]SGMInfoAggre;
	wire IsOnEdge=(pixX[9:3]==0||pixY[9:3]==0);//detect edge and reset SGM data
	//pixX[9:3]==0 : less than 7
	//pixY[9:3]==0 : less than 7
	
	
	
	
	wire [2:0]P1={SW_I[2:0]};
	wire [SGMDataDepth-1:0]P2_=P1*8;
	wire [SGMDataDepth-1:0]P2=(IsOnEdge)?0:P2_;
	genvar cti;
	generate
		for(cti=0;cti<dispLevel;cti=cti+1)
		begin:SGMLoop
			wire [censusVecW-1:0]censusDiff=PixShiftReg[cti*censusVecW+:censusVecW]^DIx1;
			
			
			//AdderTree#(.data_depth(1),.ArrL(censusVecW/2)) AT1(censusDiff[0+:censusVecW/2],LedgeSum);
			//AdderTree#(.data_depth(1),.ArrL(censusVecW/2)) AT2(censusDiff[censusVecW/2+:censusVecW/2],RedgeSum);
			//wire [$clog2(censusVecW)-1:0]LRDiff=(LedgeSum>RedgeSum)?LedgeSum-RedgeSum:RedgeSum-LedgeSum;
			
			wire [$clog2(censusVecW):0]pixDiff_;
			
			wire [$clog2(censusVecW)-1:0]LedgeSum,RedgeSum;
			
			AdderTree#(.data_depth(1),.ArrL(censusVecW)) AT2(censusDiff[0+:censusVecW],pixDiff_);
			wire [5-1:0]pixDiff=(IsOnEdge)?{5{1'b1}}:pixDiff_;

			wire [SGMDataDepth-1:0]Cs0,Cs1,Cs2,Cs3;
			if(cti==0)begin
							getMinIdx  #(.data_depth(SGMDataDepth),.ArrL(3))Cs0S
				({SGMInfo_shiftOut_dir0[cti*SGMDataDepth+:SGMDataDepth],
				SGMInfo_shiftOut_dir0[(cti+1)*SGMDataDepth+:SGMDataDepth]+P1,
				P2
				},Cs0,Cs0Index);
				
				getMinIdx  #(.data_depth(SGMDataDepth),.ArrL(3))Cs1S
				({SGMInfo_shiftOut_dir1[cti*SGMDataDepth+:SGMDataDepth],
				SGMInfo_shiftOut_dir1[(cti+1)*SGMDataDepth+:SGMDataDepth]+P1,
				P2
				},Cs1,Cs1Index);
				
				getMinIdx  #(.data_depth(SGMDataDepth),.ArrL(3))Cs2S
				({SGMInfo_shiftOut_dir2[cti*SGMDataDepth+:SGMDataDepth],
				SGMInfo_shiftOut_dir2[(cti+1)*SGMDataDepth+:SGMDataDepth]+P1,
				P2
				},Cs2,Cs2Index);
				
				getMinIdx  #(.data_depth(SGMDataDepth),.ArrL(3))Cs3S
				({SGMInfo_shiftOut_dir3[cti*SGMDataDepth+:SGMDataDepth],
				SGMInfo_shiftOut_dir3[(cti+1)*SGMDataDepth+:SGMDataDepth]+P1,
				P2
				},Cs3,Cs3Index);
			end
			else if(cti==dispLevel-1)begin
				getMinIdx  #(.data_depth(SGMDataDepth),.ArrL(3))Cs0S
				({SGMInfo_shiftOut_dir0[cti*SGMDataDepth+:SGMDataDepth],
				SGMInfo_shiftOut_dir0[(cti-1)*SGMDataDepth+:SGMDataDepth]+P1,
				P2
				},Cs0,Cs0Index);
				
				getMinIdx  #(.data_depth(SGMDataDepth),.ArrL(3))Cs1S
				({SGMInfo_shiftOut_dir1[cti*SGMDataDepth+:SGMDataDepth],
				SGMInfo_shiftOut_dir1[(cti-1)*SGMDataDepth+:SGMDataDepth]+P1,
				P2
				},Cs1,Cs1Index);
				
				getMinIdx  #(.data_depth(SGMDataDepth),.ArrL(3))Cs2S
				({SGMInfo_shiftOut_dir2[cti*SGMDataDepth+:SGMDataDepth],
				SGMInfo_shiftOut_dir2[(cti-1)*SGMDataDepth+:SGMDataDepth]+P1,
				P2
				},Cs2,Cs2Index);
				
				getMinIdx  #(.data_depth(SGMDataDepth),.ArrL(3))Cs3S
				({SGMInfo_shiftOut_dir3[cti*SGMDataDepth+:SGMDataDepth],
				SGMInfo_shiftOut_dir3[(cti-1)*SGMDataDepth+:SGMDataDepth]+P1,
				P2
				},Cs3,Cs3Index);
			end
			else begin
				getMinIdx  #(.data_depth(SGMDataDepth),.ArrL(4))Cs0S
				({SGMInfo_shiftOut_dir0[cti*SGMDataDepth+:SGMDataDepth],
				SGMInfo_shiftOut_dir0[(cti-1)*SGMDataDepth+:SGMDataDepth]+P1,
				SGMInfo_shiftOut_dir0[(cti+1)*SGMDataDepth+:SGMDataDepth]+P1,
				P2
				},Cs0,Cs0Index);
				
				getMinIdx  #(.data_depth(SGMDataDepth),.ArrL(4))Cs1S
				({SGMInfo_shiftOut_dir1[cti*SGMDataDepth+:SGMDataDepth],
				SGMInfo_shiftOut_dir1[(cti-1)*SGMDataDepth+:SGMDataDepth]+P1,
				SGMInfo_shiftOut_dir1[(cti+1)*SGMDataDepth+:SGMDataDepth]+P1,
				P2
				},Cs1,Cs1Index);
				
				getMinIdx  #(.data_depth(SGMDataDepth),.ArrL(4))Cs2S
				({SGMInfo_shiftOut_dir2[cti*SGMDataDepth+:SGMDataDepth],
				SGMInfo_shiftOut_dir2[(cti-1)*SGMDataDepth+:SGMDataDepth]+P1,
				SGMInfo_shiftOut_dir2[(cti+1)*SGMDataDepth+:SGMDataDepth]+P1,
				P2
				},Cs2,Cs2Index);
				
				getMinIdx  #(.data_depth(SGMDataDepth),.ArrL(4))Cs3S
				({SGMInfo_shiftOut_dir3[cti*SGMDataDepth+:SGMDataDepth],
				SGMInfo_shiftOut_dir3[(cti-1)*SGMDataDepth+:SGMDataDepth]+P1,
				SGMInfo_shiftOut_dir3[(cti+1)*SGMDataDepth+:SGMDataDepth]+P1,
				P2
				},Cs3,Cs3Index);
			end

			
			assign SGMInfo0[cti*SGMDataDepth+:SGMDataDepth]=
			pixDiff+Cs0;
			
			assign SGMInfo1[cti*SGMDataDepth+:SGMDataDepth]=
			pixDiff+Cs1;
			
			assign SGMInfo2[cti*SGMDataDepth+:SGMDataDepth]=
			pixDiff+Cs2;
			
			
			assign SGMInfo3[cti*SGMDataDepth+:SGMDataDepth]=
			pixDiff+Cs3;
			//(isPixMatch)?SGMData3+1+MatchScore>>2:0;
			
			
			assign SGMInfoAggre[cti*SGMDataAggreDepth+:SGMDataAggreDepth]=
			SGMInfo0[cti*SGMDataDepth+:SGMDataDepth]+
			SGMInfo1[cti*SGMDataDepth+:SGMDataDepth]+
			SGMInfo2[cti*SGMDataDepth+:SGMDataDepth]+
			SGMInfo3[cti*SGMDataDepth+:SGMDataDepth];
		end
	endgenerate
	
	
	
	
	wire [10-1:0]MinDataIdx=MinDataIdx_;
	
	
	
	wire [$clog2(dispLevel):0]MinDataIdx_;
	
	reg [(SGMDataAggreDepth)*(dispLevel)-1:0]SGMInfoAggreReg;//pipe stage 2
	always@(posedge clk_p)
		if(en_p)SGMInfoAggreReg=SGMInfoAggre;
	
	wire [SGMDataAggreDepth-1:0]MinData;
	wire [6-1:0]MinDataT=MinData[SGMDataAggreDepth-2-:6];
	
	getMinIdx  #(.data_depth(SGMDataAggreDepth ),.ArrL(dispLevel))gMI
	(
      SGMInfoAggreReg,
      MinData,MinDataIdx_
   );
	/**/
	
	
	
	
	
	
	
	
	
endmodule
