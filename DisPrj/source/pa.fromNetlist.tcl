
# PlanAhead Launch Script for Post-Synthesis floorplanning, created by Project Navigator

create_project -name VmodCAM_Ref_VGA_Split -dir "D:/Program/MYPRJ~1/HG_Sync/FPGA/fpgadisparity/DisPrj/source/planAhead_run_2" -part xc6slx45csg324-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "D:/Program/MYPRJ~1/HG_Sync/FPGA/fpgadisparity/DisPrj/source/VmodCAM_Ref.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {D:/Program/MYPRJ~1/HG_Sync/FPGA/fpgadisparity/DisPrj/source} {ipcore_dir} }
add_files [list {ipcore_dir/blkRAM2.ncf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/blkRAM_W32D640_SP.ncf}] -fileset [get_property constrset [current_run]]
add_files [list {ipcore_dir/TestIPCore.ncf}] -fileset [get_property constrset [current_run]]
set_property target_constrs_file "D:/Program/MYPRJ~1/HG_Sync/FPGA/fpgadisparity/DisPrj/source/atlys.ucf" [current_fileset -constrset]
add_files [list {atlys.ucf}] -fileset [get_property constrset [current_run]]
add_files [list {timing.ucf}] -fileset [get_property constrset [current_run]]
add_files [list {atlys_vmodcam.ucf}] -fileset [get_property constrset [current_run]]
add_files [list {hijacker.ucf}] -fileset [get_property constrset [current_run]]
link_design
